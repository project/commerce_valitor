### SUMMARY
This module provides a payment method using [VALITOR](https://www.valitor.com) platform.

### INSTALLATION
Using composer:
```
composer require drupal/commerce_valitor --sort-packages
```

### CONFIGURATION
- Go to admin/commerce/config/payment-gateways
- Add a new VALITOR payment gateway
- Edit your gateway settings and credentials.

### SPONSORS
- [Eldum rétt ehf](https://eldumrett.is)

### CONTACT
Developed and maintained by:

1xINTERNET (https://1xinternet.de)

Cambrico (http://cambrico.net).

#### Current maintainers:
- Pedro Cambra [(pcambra)](http://drupal.org/user/122101)
- Manuel Egío [(facine)](http://drupal.org/user/1169056)
- Stefan Weber [(stefanweber)](http://drupal.org/user/137384)
